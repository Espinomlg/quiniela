package com.example.espino.quiniela;

import android.os.Bundle;

/**
 * Created by espino on 14/01/17.
 */

public interface IMain {

    interface Presentador{
        void descargar(String resultados, String apuestas, boolean archivo);//true para xml, false para json
        void actualizar();
    }

    interface Vista{
        void mostrarError(String info);
        void lanzar(Bundle args);
    }
}
