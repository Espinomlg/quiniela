package com.example.espino.quiniela.modelos;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by espino on 16/01/17.
 */


public class Jornada implements Parcelable{

    public static final String TAG = "JORNADA";

    private String jornada,
    temporada;

    private double premio15,
    premio14,
    premio13,
    premio12,
    premio11,
    premio10,
    apuesta,
    recaudacion;

    private ArrayList<String> resultados;

    public Jornada(){
        resultados = new ArrayList<>();
    }

    protected Jornada(Parcel in) {
        jornada = in.readString();
        temporada = in.readString();
        premio15 = in.readDouble();
        premio14 = in.readDouble();
        premio13 = in.readDouble();
        premio12 = in.readDouble();
        premio11 = in.readDouble();
        premio10 = in.readDouble();
        apuesta = in.readDouble();
        recaudacion = in.readDouble();
        resultados = in.createStringArrayList();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(jornada);
        dest.writeString(temporada);
        dest.writeDouble(premio15);
        dest.writeDouble(premio14);
        dest.writeDouble(premio13);
        dest.writeDouble(premio12);
        dest.writeDouble(premio11);
        dest.writeDouble(premio10);
        dest.writeDouble(apuesta);
        dest.writeDouble(recaudacion);
        dest.writeStringList(resultados);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Jornada> CREATOR = new Creator<Jornada>() {
        @Override
        public Jornada createFromParcel(Parcel in) {
            return new Jornada(in);
        }

        @Override
        public Jornada[] newArray(int size) {
            return new Jornada[size];
        }
    };

    public Jornada getClone(){
        return this;
    }

    public String getJornada() {
        return jornada;
    }

    public void setJornada(String jornada) {
        this.jornada = jornada;
    }

    public String getTemporada() {
        return temporada;
    }

    public void setTemporada(String temporada) {
        this.temporada = temporada;
    }


    public void setApuesta(int apuesta) {
        this.apuesta = apuesta;
    }

    public String getResultado(int index) {
        return resultados.get(index);
    }

    public void anadirResultado(String resultado) {
        this.resultados.add(resultado);
    }

    public double getPremio15() {
        return premio15;
    }

    public void setPremio15(double premio15) {
        this.premio15 = premio15;
    }

    public double getPremio14() {
        return premio14;
    }

    public void setPremio14(double premio14) {
        this.premio14 = premio14;
    }

    public double getPremio13() {
        return premio13;
    }

    public void setPremio13(double premio13) {
        this.premio13 = premio13;
    }

    public double getPremio12() {
        return premio12;
    }

    public void setPremio12(double premio12) {
        this.premio12 = premio12;
    }

    public double getPremio11() {
        return premio11;
    }

    public void setPremio11(double premio11) {
        this.premio11 = premio11;
    }

    public double getPremio10() {
        return premio10;
    }

    public void setPremio10(double premio10) {
        this.premio10 = premio10;
    }

    public double getApuesta() {
        return apuesta;
    }

    public void setApuesta(double apuesta) {
        this.apuesta = apuesta;
    }

    public double getRecaudacion() {
        return recaudacion;
    }

    public void setRecaudacion(double recaudacion) {
        this.recaudacion = recaudacion;
    }

    public int getLength(){
        return resultados.size();
    }

    public double getPremios(){
        return premio10 + premio11 + premio12 + premio13 + premio14 + premio15;
    }

    public String getResultados(){
        String resultado = "";
        for(String res : resultados){
            resultado = resultado.concat(res);
        }

        return resultado;
    }
}
