package com.example.espino.quiniela;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.espino.quiniela.modelos.Premio;

import java.util.List;

/**
 * Created by espino on 17/01/17.
 */
public class PremiosAdapter extends ArrayAdapter<Premio>{


    public PremiosAdapter(Context context, List<Premio> objects) {
        super(context, R.layout.listitem_premio, objects);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PremioHolder holder = null;

        if(convertView == null){
            holder = new PremioHolder();
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_premio, parent, false);

            holder.apuesta = (TextView) convertView.findViewById(R.id.listitem_apuesta);
            holder.premio = (TextView) convertView.findViewById(R.id.listitem_premio);

            convertView.setTag(holder);
        }
        else
            holder = (PremioHolder) convertView.getTag();

        holder.apuesta.setText(getItem(position).getApuesta());
        holder.premio.setText(String.valueOf(getItem(position).getPremio()));

        return convertView;
    }


    private static class PremioHolder{
        private TextView apuesta,premio;
    }
}
