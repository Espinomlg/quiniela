package com.example.espino.quiniela.vistas;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.espino.quiniela.Analizar;
import com.example.espino.quiniela.NetworkController;
import com.example.espino.quiniela.PremiosAdapter;
import com.example.espino.quiniela.R;
import com.example.espino.quiniela.VolleySingleton;
import com.example.espino.quiniela.modelos.Jornada;
import com.example.espino.quiniela.modelos.Premios;

import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by espino on 17/01/17.
 */

public class PremiosActivity extends AppCompatActivity {

    private TextView jornada, temporada, total, empty;
    private ListView lista;

    private PremiosAdapter adapter;
    private Bundle bnd;
    private Premios premios;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_premios);

        jornada = (TextView) findViewById(R.id.premios_txv_jornada);
        temporada = (TextView) findViewById(R.id.premios_txv_temporada);
        total = (TextView) findViewById(R.id.premios_txv_total);
        lista = (ListView) findViewById(android.R.id.list);
        empty = (TextView) findViewById(android.R.id.empty);

        lista.setEmptyView(empty);

        bnd = getIntent().getBundleExtra(MainActivity.TAG);

        premios =  bnd.getParcelable(Premios.TAG);
        Jornada jor = bnd.getParcelable(Jornada.TAG);
        jornada.setText(String.format(getResources().getString(R.string.txv_jornada), jor.getJornada()));
        temporada.setText(String.format(getResources().getString(R.string.txv_temporada), jor.getTemporada()));
        total.setText(String.format(getResources().getString(R.string.txv_total), premios.getTotal()));

        adapter = new PremiosAdapter(getApplicationContext(), premios.getList());
        lista.setAdapter(adapter);

        try {
            subir(bnd.getBoolean(MainActivity.TAG_EXTENSION));
        } catch (IOException e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void subir(boolean mode) throws IOException, JSONException {
        if(mode)
            subirPremiosXML();
        else
            subirPremiosJSON();
    }


    private void subirPremiosXML() throws IOException{
        final String contenido = Analizar.crearPremiosXML(premios);
        final String name = bnd.getString(MainActivity.TAG_PREMIOS);
        StringRequest request = new StringRequest
                (Request.Method.POST, NetworkController.WRITE, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),"Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("password", "1234");
                params.put("name", name);
                params.put("content", contenido);

                return params;
            }
        };


        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);

        request.setTag(VolleySingleton.TAG);
        request.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 1));
    }

    private void subirPremiosJSON() throws JSONException {
        final String contenido = Analizar.crearPremiosJSON(premios);
        final String name = bnd.getString(MainActivity.TAG_PREMIOS);
        StringRequest request = new StringRequest
                (Request.Method.POST, NetworkController.WRITE, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),"Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("password", "1234");
                params.put("name", name);
                params.put("content", contenido);

                return params;
            }
        };


        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);

        request.setTag(VolleySingleton.TAG);
        request.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 1));
    }

}
