package com.example.espino.quiniela.modelos;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by espino on 17/01/17.
 */

public class Premios implements Parcelable{

    public static final String TAG = "PREMIOS";

    private ArrayList<Premio> lista;

    public Premios(){
        lista = new ArrayList<>();
    }

    protected Premios(Parcel in) {
        lista = in.createTypedArrayList(Premio.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(lista);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Premios> CREATOR = new Creator<Premios>() {
        @Override
        public Premios createFromParcel(Parcel in) {
            return new Premios(in);
        }

        @Override
        public Premios[] newArray(int size) {
            return new Premios[size];
        }
    };

    public void anadir(Premio p){
        lista.add(p);
    }

    public Premio getPremio(int index){
        return lista.get(index);
    }

    public int length(){
        return lista.size();
    }

    public List<Premio> getList(){
        return lista;
    }

    public double getTotal(){
        double total = 0;
        for(Premio premio: lista){
            total += premio.getPremio();
        }

        return total;
    }


}
