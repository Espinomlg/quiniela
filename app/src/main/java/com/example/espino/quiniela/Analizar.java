package com.example.espino.quiniela;


import android.util.Xml;

import com.example.espino.quiniela.modelos.Jornada;
import com.example.espino.quiniela.modelos.Premio;
import com.example.espino.quiniela.modelos.Premios;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.text.DecimalFormat;


/**
 * Created by espino on 14/01/17.
 */

public class Analizar {


    public static String crearJSONdeXML(String response) throws XmlPullParserException,JSONException, IOException {
        JSONObject root = new JSONObject();
        JSONObject main = new JSONObject();
        JSONObject objQuiniela = new JSONObject();
        JSONObject objPartidos = new JSONObject();
        JSONArray quiniela = new JSONArray();
        JSONArray partidos = new JSONArray();
        XmlPullParser xpp = Xml.newPullParser();
        xpp.setInput(new StringReader(response));
        int eventType = xpp.next();

        while(eventType != XmlPullParser.END_DOCUMENT){
            switch (eventType){
                case XmlPullParser.START_TAG:
                    if(xpp.getName().compareTo("quinielista") == 0)
                        main.put("error", xpp.getAttributeValue(0));
                    else if(xpp.getName().compareTo("quiniela") == 0){
                        for(byte i = 0; i < xpp.getAttributeCount(); i++){
                            objQuiniela.put(xpp.getAttributeName(i), xpp.getAttributeValue(i));
                        }
                    }
                    else if(xpp.getName().compareTo("partit") == 0){
                        for(byte i = 0; i < xpp.getAttributeCount(); i++){
                            objPartidos.put(xpp.getAttributeName(i),xpp.getAttributeValue(i));
                        }
                    }
                    break;

                case XmlPullParser.END_TAG:
                    if(xpp.getName().compareTo("partit") == 0) {
                        partidos.put(objPartidos);
                        objPartidos = new JSONObject();
                    }
                    else if(xpp.getName().compareTo("quiniela") == 0){
                        objQuiniela.put("partit",partidos);
                        partidos = new JSONArray();
                        quiniela.put(objQuiniela);
                        objQuiniela = new JSONObject();
                    }
                    else if(xpp.getName().compareTo("quinielista") == 0){
                        main.put("quiniela", quiniela);
                        root.put("quinielista",main);
                    }
                    break;
            }
            eventType = xpp.next();
        }
      return root.toString();
    }

    public static Jornada analizarXML(String response) throws XmlPullParserException, IOException{
        boolean continuar = true;
        Jornada temp = new Jornada();
        Jornada jor = new Jornada();
        XmlPullParser xpp = Xml.newPullParser();
        xpp.setInput(new StringReader(response));
        int eventType = xpp.next();

        while(eventType != XmlPullParser.END_DOCUMENT && continuar){
            switch (eventType){
                case XmlPullParser.START_TAG:
                    if(xpp.getName().compareTo("quiniela") == 0){
                        for(byte i = 0; i < xpp.getAttributeCount(); i++){
                            if(xpp.getAttributeName(i).compareTo("jornada") == 0)
                                temp.setJornada(xpp.getAttributeValue(i)
                                );
                            else if(xpp.getAttributeName(i).compareTo("temporada") == 0)
                                temp.setTemporada(xpp.getAttributeValue(i));
                            else if(xpp.getAttributeName(i).compareTo("recaudacion") == 0)
                                temp.setRecaudacion(Double.valueOf(xpp.getAttributeValue(i)) / 100.0);
                            else if(xpp.getAttributeName(i).compareTo("el15") == 0) {
                                temp.setPremio15(Double.valueOf(xpp.getAttributeValue(i))/ 100.0);
                            }
                            else if(xpp.getAttributeName(i).compareTo("el14") == 0) {
                                temp.setPremio14(Double.valueOf(xpp.getAttributeValue(i)) / 100.0);
                            }
                            else if(xpp.getAttributeName(i).compareTo("el13") == 0) {
                                temp.setPremio13(Double.valueOf(xpp.getAttributeValue(i)) / 100.0);
                            }
                            else if(xpp.getAttributeName(i).compareTo("el12") == 0) {
                                temp.setPremio12(Double.valueOf(xpp.getAttributeValue(i)) / 100.0);
                            }
                            else if(xpp.getAttributeName(i).compareTo("el11") == 0) {
                                temp.setPremio11(Double.valueOf(xpp.getAttributeValue(i)) / 100.0);
                            }
                            else if(xpp.getAttributeName(i).compareTo("el10") == 0) {
                                temp.setPremio10(Double.valueOf(xpp.getAttributeValue(i)) / 100.0);
                                if(temp.getPremios() == 0)
                                    continuar = false;
                            }
                        }
                    }
                    else if(xpp.getName().compareTo("partit") == 0){
                        temp.anadirResultado(xpp.getAttributeValue(xpp.getAttributeCount() - 1));
                    }
                    break;
                case XmlPullParser.END_TAG:
                    if(xpp.getName().compareTo("quiniela") == 0) {
                        jor = temp.getClone();
                        temp = new Jornada();
                    }
                    break;
            }
            eventType = xpp.next();
        }

        return jor;
    }

    public static Jornada analizarJson(JSONObject response) throws JSONException{
        boolean continuar = true;
        Jornada jor = null;
        JSONObject object = response.getJSONObject("quinielista");
        JSONArray quiniela = object.getJSONArray("quiniela");
        JSONObject item;
        for(byte i = 0; i < quiniela.length() && continuar; i++){
            item = quiniela.getJSONObject(i);
            double suma = item.getDouble("el15") + item.getDouble("el14") + item.getDouble("el13") + item.getDouble("el12") + item.getDouble("el11") + item.getDouble("el10");
            if(suma != 0) {
                jor = new Jornada();
                jor.setJornada(item.getString("jornada"));
                jor.setTemporada(item.getString("temporada"));
                jor.setRecaudacion(item.getDouble("recaudacion") / 100.0);
                jor.setPremio15(item.getDouble("el15") / 100.0);
                jor.setPremio14(item.getDouble("el14") / 100.0);
                jor.setPremio13(item.getDouble("el13") / 100.0);
                jor.setPremio12(item.getDouble("el12") / 100.0);
                jor.setPremio11(item.getDouble("el11") / 100.0);
                jor.setPremio10(item.getDouble("el10") / 100.0);
                JSONArray partidos = item.getJSONArray("partit");
                JSONObject itemPartidos;
                for(byte index = 0; index < partidos.length(); index++){
                    itemPartidos = partidos.getJSONObject(index);
                    jor.anadirResultado(itemPartidos.getString("sig"));
                }

            }
            else
                continuar = false;

        }

        return jor;
    }

    public static Premios calcular(Jornada jor, StringBuffer  apuestas){
        final int pleno = 15;
        String[] array = apuestas.toString().split("\n");
        Premios premios = new Premios();
        byte errores = 0;


        for(String apuesta: array) {
            String normal = apuesta.substring(0, 14);
            String al15 = apuesta.substring(14);

            for (int i = 0; i < normal.length() && errores < 6; i++) {
                if (apuesta.charAt(i) != jor.getResultado(i).charAt(0))
                    errores++;
            }
            if(al15.compareTo(jor.getResultado(jor.getLength() - 1)) != 0)
                errores++;

            if (errores < 6) {
                switch (pleno - errores) {
                    case 15:
                        premios.anadir(new Premio(apuesta, jor.getPremio15()));
                        break;
                    case 14:
                        premios.anadir(new Premio(apuesta, jor.getPremio14()));
                        break;
                    case 13:
                        premios.anadir(new Premio(apuesta, jor.getPremio13()));
                        break;
                    case 12:
                        premios.anadir(new Premio(apuesta, jor.getPremio12()));
                        break;
                    case 11:
                        premios.anadir(new Premio(apuesta, jor.getPremio11()));
                        break;
                    case 10:
                        premios.anadir(new Premio(apuesta, jor.getPremio10()));
                        break;
                }
            }
            errores = 0;
        }

        return premios;
    }

    public static String crearPremiosXML(Premios premios) throws IOException {
        String xml;
        DecimalFormat format = new DecimalFormat(".00");
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        XmlSerializer serializer = Xml.newSerializer();
        serializer.setOutput(out, "UTF-8");
        serializer.startDocument("UTF-8", true);
        serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true); //poner tabulación
        serializer.startTag(null, "Premios");
        serializer.attribute(null,"total", format.format(premios.getTotal()));
        for (int i = 0; i < premios.length(); i++) {
            serializer.startTag(null,"apuesta");
            serializer.startTag(null, "partidos");
            serializer.text(premios.getPremio(i).getApuesta());
            serializer.endTag(null,"partidos");
            serializer.startTag(null,"premio");
            serializer.text(String.valueOf(premios.getPremio(i).getPremio()));
            serializer.endTag(null,"premio");
            serializer.endTag(null,"apuesta");
        }
        serializer.endTag(null, "Premios");
        serializer.endDocument();
        serializer.flush();
        xml = out.toString("UTF-8");
        out.flush();

        return xml;
    }

    public static String crearPremiosJSON(Premios premios) throws JSONException {
        DecimalFormat format = new DecimalFormat(".00");
        JSONObject root = new JSONObject();
        JSONObject apuesta;
        JSONArray apuestas = new JSONArray();
        root.put("total", format.format(premios.getTotal()));
        for(int i = 0; i < premios.length(); i++){
            apuesta = new JSONObject();
            apuesta.put("partidos", premios.getPremio(i).getApuesta());
            apuesta.put("premio", premios.getPremio(i).getPremio());
            apuestas.put(apuesta);
        }
        root.put("premios", apuestas);


        return root.toString();
    }

}
