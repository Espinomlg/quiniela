package com.example.espino.quiniela.vistas;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.espino.quiniela.Analizar;
import com.example.espino.quiniela.IMain;
import com.example.espino.quiniela.NetworkController;
import com.example.espino.quiniela.R;
import com.example.espino.quiniela.modelos.Jornada;
import com.example.espino.quiniela.modelos.Premios;

public class MainActivity extends AppCompatActivity implements IMain.Vista {



    public static final String TAG_PREMIOS = "NOMBRE_PREMIOS";
    public static final String TAG_EXTENSION = "MODO";
    public static final String TAG = "MAINACTIVITY";

    private RadioGroup archivo;
    private TextInputLayout apuestas, resultados, premios;
    private IMain.Presentador presentador;
    private boolean modo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(isNetworkAvailable()) {
            presentador = new NetworkController(this);

            archivo = (RadioGroup) findViewById(R.id.main_rdgroup);
            apuestas = (TextInputLayout) findViewById(R.id.main_txi_apuestas);
            resultados = (TextInputLayout) findViewById(R.id.main_txi_resultados);
            premios = (TextInputLayout) findViewById(R.id.main_txi_premios);
            modo = true;

            archivo.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == R.id.main_rdbtn_json)
                        modo = false;
                    else
                        modo = true;
                }
            });

            presentador.actualizar();
        }
        else{
            AlertDialog.Builder popup = new AlertDialog.Builder(getApplicationContext());
            popup.setTitle("Problema de conexión");
            popup.setMessage("No hay conexión a internet, la aplicación va a cerrarse");
            popup.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
        }
    }

    public void calcular(View v){
        String urlApuestas = apuestas.getEditText().getText().toString(),
                urlResultados = resultados.getEditText().getText().toString(),
                nombrePremios = premios.getEditText().getText().toString();

        if(!TextUtils.isEmpty(urlApuestas) && !TextUtils.isEmpty(urlResultados) && !TextUtils.isEmpty(nombrePremios))
            presentador.descargar(urlResultados, urlApuestas, modo);
    }

    @Override
    public void mostrarError(String info) {
            Toast.makeText(getApplicationContext(), "Ha ocurrido un error: " + info, Toast.LENGTH_LONG).show();
    }

    @Override
    public void lanzar(Bundle args) {
        Premios p = Analizar.calcular((Jornada) args.getParcelable(Jornada.TAG),(StringBuffer)args.getSerializable(NetworkController.TAG_APUESTAS));
        args.putString(TAG_PREMIOS, premios.getEditText().getText().toString());
        args.putParcelable(Premios.TAG, p);
        args.putBoolean(TAG_EXTENSION, modo);
        Intent i = new Intent(getApplicationContext(), PremiosActivity.class);
        i.putExtra(TAG,args);
        startActivity(i);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }
}
