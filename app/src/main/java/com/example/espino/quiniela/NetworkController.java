package com.example.espino.quiniela;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;


import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.espino.quiniela.modelos.Jornada;

import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by espino on 14/01/17.
 */

public class NetworkController implements IMain.Presentador {
//todo arreglar el booleano
    public static final String TAG_APUESTAS = "APUESTAS";

    private static final String QUINIELISTA = "https://www.quinielista.es/xml/temporada.asp";
    private static final String RESULTADOS = "resultados";
    private static final String EXTENSION_XML = ".xml";
    private static final String EXTENSION_JSON = ".json";
    public static final String WRITE = "http://alumno.club/superior/andres/write.php";
    private static final String PROTOCOLO = "http://";
    private static final byte DESCARGADO = 0;

    private IMain.Vista vista;
    private Jornada jor;
    private StringBuffer resultados;
    private boolean descargado;
    private Bundle bnd;
    private static byte condicion;

    public NetworkController(IMain.Vista vista) {
        this.vista = vista;
        jor = new Jornada();
        descargado = false;
        bnd = new Bundle();
        condicion = 0;
    }

    @Override
    public void descargar(String resultados, String apuestas, boolean archivo) {
        if(archivo)
            descargarResultadosXML(resultados,apuestas);
        else
            descargarResultadosJSON(resultados,apuestas);


    }

    private void descargarResultadosXML(String nombre, final String apuestas) {
        StringRequest request = new StringRequest
                (Request.Method.GET, PROTOCOLO + nombre, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {
                            jor = Analizar.analizarXML(response);
                            bnd.putParcelable(Jornada.TAG, jor);
                            descargarApuestas(apuestas);

                        } catch (XmlPullParserException e) {
                            vista.mostrarError(e.getMessage());
                            descargado = false;
                        } catch (IOException e) {
                            vista.mostrarError(e.getMessage());
                            descargado = false;
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        vista.mostrarError(error.getMessage() + "1");
                        descargado = false;
                    }
                });


        VolleySingleton.getInstance(((AppCompatActivity) vista).getApplicationContext()).addToRequestQueue(request);

        request.setTag(VolleySingleton.TAG);
        request.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 1));
    }
    private void descargarResultadosJSON(String nombre, final String apuestas) {
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, PROTOCOLO + nombre, null, new Response.Listener<JSONObject>() {


                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            jor = Analizar.analizarJson(response);
                            bnd.putParcelable(Jornada.TAG, jor);
                            descargarApuestas(apuestas);


                        } catch (JSONException e) {
                            vista.mostrarError(e.getMessage());
                            descargado = false;
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        vista.mostrarError(error.getMessage());
                        descargado = false;
                    }
                });

        VolleySingleton.getInstance(((AppCompatActivity) vista).getApplicationContext()).addToRequestQueue(jsObjRequest);

        jsObjRequest.setTag(VolleySingleton.TAG);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 1));
    }
    private void descargarApuestas(String nombre) {
        StringRequest request = new StringRequest
                (Request.Method.GET, PROTOCOLO + nombre, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        resultados = new StringBuffer(response);
                        bnd.putSerializable(TAG_APUESTAS, resultados);
                        vista.lanzar(bnd);


                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        vista.mostrarError(error.getMessage() + "2");
                        descargado = false;
                    }
                });


        VolleySingleton.getInstance(((AppCompatActivity) vista).getApplicationContext()).addToRequestQueue(request);

        request.setTag(VolleySingleton.TAG);
        request.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 1));

    }

    @Override
    public void actualizar() {
        StringRequest xmlrequest = new StringRequest
                (Request.Method.GET, QUINIELISTA, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {
                            subir(RESULTADOS + EXTENSION_XML, response);
                            subir(RESULTADOS + EXTENSION_JSON, Analizar.crearJSONdeXML(response));

                        } catch (XmlPullParserException e) {
                            vista.mostrarError(e.getMessage());
                        } catch (JSONException e) {
                            vista.mostrarError(e.getMessage());
                        } catch (IOException e) {
                            vista.mostrarError(e.getMessage());
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        vista.mostrarError(error.getMessage());
                    }
                });

        VolleySingleton.getInstance(((AppCompatActivity) vista).getApplicationContext()).addToRequestQueue(xmlrequest);

        xmlrequest.setTag(VolleySingleton.TAG);
        xmlrequest.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 1));
    }


    private void subir(String file, String content) {
        final String name = file;
        final String contenido = content;
        StringRequest request = new StringRequest
                (Request.Method.POST, WRITE, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                    }

                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        vista.mostrarError(error.getMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("password", "1234");
                params.put("name", name);
                params.put("content", contenido);

                return params;
            }
        };


        VolleySingleton.getInstance(((AppCompatActivity) vista).getApplicationContext()).addToRequestQueue(request);

        request.setTag(VolleySingleton.TAG);
        request.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 1));
    }

}
