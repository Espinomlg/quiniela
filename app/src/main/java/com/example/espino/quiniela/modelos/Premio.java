package com.example.espino.quiniela.modelos;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by espino on 17/01/17.
 */

public class Premio implements Parcelable{

    private String apuesta;
    private double premio;

    public Premio(String apuesta, double premio){
        this.apuesta = apuesta;
        this.premio = premio;
    }

    protected Premio(Parcel in) {
        apuesta = in.readString();
        premio = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(apuesta);
        dest.writeDouble(premio);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Premio> CREATOR = new Creator<Premio>() {
        @Override
        public Premio createFromParcel(Parcel in) {
            return new Premio(in);
        }

        @Override
        public Premio[] newArray(int size) {
            return new Premio[size];
        }
    };

    public String getApuesta() {
        return apuesta;
    }

    public void setApuesta(String apuesta) {
        this.apuesta = apuesta;
    }

    public double getPremio() {
        return premio;
    }

    public void setPremio(double premio) {
        this.premio = premio;
    }

}
